import labo

def main_labo():
    '''
    Affiche le menu d'arrivée d'un utilisateur si il souhaite ajouter une personne.
    -Exemples: 
        -Arrivée de Xavier en F305
        ...
    '''
    
    laboratoire= labo.labo_vide() #Initialisation du laboratoire vide.

    quitter= False

    while not quitter:

        #Affichage du menu d'entrée:
        print("""Que souhaitez-faire ?    
            1-Enregistrer une Arrivée   
            2-Enregistrer un Départ 
            3-Modifier Bureau Occupé  
            4-Changer de Nom 
            5-Savoir si un Nom est Membre du Labo 
            6-Connaître le Numéro de Bureau d'une Personne 
            7-Listing du Personnel et des Bureaux Occupés
            8-Occupation des bureaux
            9-Occupation des bureaux en html
            10-Occupation des bureaux en json
            11-Exportation Occupation des bureaux en csv
            12-Importation Occupation des bureaux en csv
            """)


        #Demande le choix du user 
        choix= int(input("Quel choix 1, 2, 3, 4, 5, 6, 7, 8 , 9, 10, 11, 12 ?"))    

        if choix == 1: #ARRIVEE 
    
            nom= input("Quelle est le nom du nouvel arrivant ? ") #Demande du nom du nouvel occupant

            bureau= input("Quelle est le numéro de bureau du nouvel arrivant ? ") #Demande du numéro de bureau du nouvel occupant

            #Exception levée pour une personne déjà présente
            try:
                labo.enregistrer_arrive(laboratoire, nom, bureau)
            except labo.PresentException:
                print("Déjà Présent !")


        elif choix ==2: #DEPART

            nom= input("Quel est le nom du partant ? ") 

            #Exception levée pour une personne déjà absente
            try:
                labo.enregistrer_depart(laboratoire, nom) 
            except labo.AbsentException:
                print("Déjà Absent !")


        elif choix == 3: #MODIFICATION BUREAU

            nom= input(" A quelle personne souhaitez-vous modifier le bureau ? ")

            bureau= input("Entrez le nouveau numéro du bureau: ")
            #Exception levée pour une personne qui n'est pas présente 
            try:
                labo.modifier_bureau(laboratoire,nom,bureau)
            except labo.AbsentException:
                print("Cette personne n'existe pas !")


        elif choix == 4: #MODIFICATION NOM

            ancien_nom= input("Quel nom souhaitez-vous modifier ? ")

            nouveau_nom= input("Entrez le nouveau nom: ")

            try:
                labo.change_nom(laboratoire,ancien_nom, nouveau_nom)
            except labo.AbsentException:
                print("Cette personne n'existe pas !")

        elif choix == 5: #SAVOIR SI MEMBRE DU LABO

            nom= input("Entrez un nom pour savoir si il est membre du labo: ")

            if labo.membre_labo(laboratoire,nom):
                print(nom , " est membre du labo")
            else : 
                print("Cette personne n'est pas membre du labo !")

        elif choix == 6: #CONNAITRE NUMERO DE BUREAU 

            nom= input("Entrez un nom pour connaître son numéro de bureau: ")

            try:
                print(labo.obtenir_bureau(laboratoire,nom))
            except labo.AbsentException:
                print("Cette personne n'existe pas !") 

        elif choix == 7: #LISTING LABORATOIRE

            print(laboratoire)

        elif choix == 8: #OCCUPATION DES BUREAUX

            labo.occupation_bureau(laboratoire)

        elif choix == 9: #OCCUPATION DES BUREAUX EN HTML

            labo.occupation_bureau_html(laboratoire)

        elif choix == 10: #OCCUPATION DES BUREAUX EN JSON

            labo.occupation_bureau_json(laboratoire)

        elif choix == 11: #Exportation OCCUPATION DES BUREAUX EN CSV
            
            labo.exportation_occupation_bureau_csv(laboratoire)

        elif choix == 12: #IMPORTATION OOCUPATION DES BUREAUX CSV

            laboratoire = labo.importation_bureau_csv(laboratoire)
        
        else:
            print("Erreur !")

        

main_labo()






