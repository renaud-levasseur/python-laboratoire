import json
import csv


class LaboException(Exception):
    '''
    Géneralise les exceptions du laboratoire.
    '''
    pass


class AbsentException(LaboException):
    pass


class PresentException(LaboException):
    pass



def labo_vide():
    '''
    Création d'un laboratoire vide.
    :return: laboratoire vide
    '''
    laboratoire= dict()
    return laboratoire



def enregistrer_arrive(labo, nom, bureau):
    '''
    Enregistre les arrivées dans le labo nom et bureau.
    '''
    if nom in labo: #Si le nom est déjà present 
        raise PresentException(nom)
    labo[nom]= bureau
    
    

def enregistrer_depart(labo, nom):
    '''
    Enregistre le départ d'une personne d'un bureau.
    '''
    if nom not in labo: #Si le nom n'est pas présent.
        raise AbsentException(nom) 

    del labo[nom]
        

def modifier_bureau(labo, nom, bureau):
    '''
    Modifie le bureau occupé par une personne.
    '''
    if nom not in labo: #Si le nom n'existe pas 
        raise AbsentException(nom)

    labo[nom]= bureau 



def change_nom(labo, ancien_nom, nouveau_nom):
    '''
    Change le nom d'une personne du laboratoire.
    '''
    if ancien_nom not in labo: #Si le nom n'existe pas 
        raise AbsentException(ancien_nom)

    labo[nouveau_nom]= labo[ancien_nom]
    del labo[ancien_nom]
    
    

def membre_labo(labo, nom):
    '''
    Affiche si le membre du labo est présent.
    '''

    if nom not in labo: #Si le nom n'est pas présent 
        return False
    
    if nom in labo: 
        print("Cette personne est bien membre du laboratoire")
        return True


def obtenir_bureau(labo, nom):
    '''
    Affiche le numéro de bureau d'une personne.
    '''

    if nom not in labo:
        raise AbsentException(nom)
    
    if nom in labo:
        #print(labo[nom]) 
        return labo[nom]



def occupation_bureau(labo):
    '''
    Affiche les bureaux occupés.
    '''
    if labo == {}:
        print("Le labo est vide")
    else :
        nouvDict= {}    
        for cle, valeur in labo.items():
            #cle : Jean
            #valeur : F103
            if not valeur in nouvDict:
                #on créé une nouvelle liste au bureau
                nouvDict.update({valeur:[cle]})
            else:
                #on ajoute a la liste du bureau
                nouvDict[valeur].append(cle)

        for cle in sorted(nouvDict.keys()):
            print(cle + ":")
            nouvDict[cle].sort()
            for nom in nouvDict[cle]:
                print("-" + nom)



def occupation_bureau_html(labo):
    '''
    Enregistre l'occupation des bureaux dans un fichier html
    '''
    print("Enregistrement dans labo.html...")
    fichier = open("labo.html","w")
    fichier.write("""
    <!DOCTYPE html>
    <html>
    <head>
    <meta charset="utf-8">
    <title> Affichage des bureaux</title>
    </head>
    <body>
    """


    )
    if labo == {}:

        fichier.write("""  
        Le labo est vide !  
        </body>
        </html>
        """)
        fichier.close()
        print("Enregistrement terminé")
    else :
        nouvDict= {}    
        for cle, valeur in labo.items():
            #cle : Jean
            #valeur : F103
            if not valeur in nouvDict:
                #Crée une nouvelle liste au bureau
                nouvDict.update({valeur:[cle]})
            else:
                #Ajoute a la liste du bureau
                nouvDict[valeur].append(cle)


        for cle in sorted(nouvDict.keys()):
            nouvDict[cle].sort()
            fichier.write(cle + ": <br>")
            for nom in nouvDict[cle]:
                fichier.write("-" + nom +"<br>")

        fichier.write("""  
        </body>
        </html>
        """)
        fichier.close()
        print("Enregistrement terminé")


def occupation_bureau_json(labo):
    if labo == {}:
        print("Le labo est vide")
    else :
        nouvDict= {}    
        for cle, valeur in labo.items():
            if valeur not in nouvDict:
                nouvDict.update({valeur:[cle]})
            else:                
                nouvDict[valeur].append(cle)

    data = str(nouvDict)
    print("Ecriture dans labo.json")
    with open("labo.json","w") as write_file:
        json.dump(data,write_file)




def exportation_occupation_bureau_csv(labo):
    if labo == {}:
        print("Le labo est vide")
    else :
        nouvDict= {}    
        for cle, valeur in labo.items():
            if not valeur in nouvDict:
                nouvDict.update({valeur:[cle]})
            else:
                nouvDict[valeur].append(cle)

    with open("labo.csv",'w', newline= '') as csvfile:
        labowriter= csv.writer(csvfile, delimiter= ',', quotechar='|', quoting=csv.QUOTE_MINIMAL)
        labowriter.writerow(['Bureau', 'Nom'])
        for cle in nouvDict:
            listeAssociee = nouvDict[cle]
            for nom in listeAssociee:
                labowriter.writerow([cle,nom])


     

def importation_bureau_csv(ancienLabo):
   
    laboratoire = ancienLabo
    with open('labo.csv', newline= '') as cvsfile:
        reader= csv.DictReader(cvsfile)
        for row in reader:
            
            bureau= row['Bureau'] 
            nom= row['Nom'] 
            
            if nom in laboratoire and bureau != laboratoire[nom]:
                print(nom + "est déjà présent en " + laboratoire[nom])
            else: 
                laboratoire.update({nom:bureau})
    print(laboratoire)
    return laboratoire