import pytest
import labo




def test_enregistrer_arrive():
    laboratoire= labo.labo_vide()
    labo.enregistrer_arrive(laboratoire, "Jean", "F103")
    assert laboratoire == {"Jean": "F103"}
    



def test_enregistrer_depart():
    laboratoire= labo.labo_vide()
    labo.enregistrer_arrive(laboratoire, "Jean", "F103")
    labo.enregistrer_depart(laboratoire, "Jean")
    assert laboratoire == {}
    



def test_modifier_bureau():
    laboratoire= labo.labo_vide()
    labo.enregistrer_arrive(laboratoire,"Jean", "F103")    
    labo.modifier_bureau(laboratoire,"Jean", "F109")
    assert laboratoire["Jean"] == "F109" 
    



def test_change_nom():
    laboratoire = labo.labo_vide()
    labo.enregistrer_arrive(laboratoire, "Jean","F103")
    labo.change_nom(laboratoire, "Jean", "Pierre")
    assert "Pierre" in laboratoire
    assert "Jean" not in laboratoire
    assert laboratoire["Pierre"]=="F103"
    


def test_membre_labo():
    laboratoire= labo.labo_vide()
    labo.enregistrer_arrive(laboratoire, "Jean", "F103")  
    assert labo.membre_labo(laboratoire, "Jean") 
    assert not (labo.membre_labo(laboratoire, "Pierre"))
    


def test_obtenir_bureau():
    laboratoire= labo.labo_vide()
    labo.enregistrer_arrive(laboratoire, "Jean", "F103")
    assert labo.obtenir_bureau(laboratoire,"Jean") == "F103"
   

