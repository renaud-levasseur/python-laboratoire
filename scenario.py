
import labo 




def scenario():

    laboratoire= labo.labo_vide()
    labo.enregistrer_arrive(laboratoire, "Jean", "F103")
    print(laboratoire)
    labo.change_nom(laboratoire, "Jean", "Geralt")
    print(laboratoire)
    labo.membre_labo(laboratoire, "Geralt")
    labo.modifier_bureau(laboratoire, "Geralt", "F109")
    print(laboratoire)
    labo.obtenir_bureau(laboratoire, "Geralt")
    labo.enregistrer_depart(laboratoire, "Geralt")
    print(laboratoire)

scenario()